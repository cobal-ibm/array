       IDENTIFICATION DIVISION.
       PROGRAM-ID. Month-Inc.



       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-PROV-INPUT ASSIGN TO "PROV.DAT"
           ORGANIZATION IS SEQUENTIAL
           FILE STATUS IS WS-PROV-INPUT-STATUS.
           
           SELECT 200-MONTH-INPUT  ASSIGN TO "MONTHR.DAT"
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS WS-MONTH-INPUT-STATUS.
           
           SELECT 300-INC-OUTPUT  ASSIGN TO "INCOME.RPT"
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS WS-INC-OUTPUT-STATUS.


       DATA DIVISION. 
       FILE SECTION. 
       FD  100-PROV-INPUT
           BLOCK CONTAINS 0 RECORDS.

       01  PROV-INPUT-RECORD.
           05 PRO-CODE                PIC X(3).
           05 PRO-NAME                PIC X(20).
           05 FILLER                  PIC X(7).
       
       
       FD  200-MONTH-INPUT
           BLOCK CONTAINS 0 RECORDS.

       01  MONTH-INPUT-RECORDS.
           05 MONTH-NUM               PIC X(2).
           05 FILLER                  PIC X(5).
           05 MONTH-PRO-CODE          PIC X(3).
           05 FILLER                  PIC X(7).
           05 MONTH-INC               PIC X(12).

       FD  300-INC-OUTPUT
           BLOCK CONTAINS 0 RECORDS.
       
       01  INC-OUTPUT-RECORD         PIC X(80).
           

       WORKING-STORAGE SECTION. 
       01  WS-PROV-INPUT-STATUS       PIC X(2).
           88 FILE-OK                 VALUE "00".
           88 FILE-AT-END             VALUE "10".

       01  WS-MONTH-INPUT-STATUS      PIC X(2).
           88 FILE-OK                 VALUE "00".
           88 FILE-AT-END             VALUE "10".

       01  WS-INC-OUTPUT-STATUS       PIC X(2).
           88 FILE-OK                 VALUE "00".
           88 FILE-AT-END             VALUE "10".

       01  WS-CAL.
           05 WS-PROV-INPUT-COUNT     PIC 9(5) VALUE ZERO.
           05 WS-MONTH-INPUT-COUNT    PIC 9(5) VALUE ZERO.
           
      *    05 WS-IDX-PRO              PIC 9(5) VALUE 1.
           05 WS-PROVINCE OCCURS 6 TIMES INDEXED BY IDX-PRO.
              10 WS-PRO-CODE                PIC X(3).
              10 WS-PRO-NAME                PIC X(20).
              10 WS-INCOME-TOTAL            PIC 9(9)V99 VALUE ZERO.

           05 WS-MONTH.
              10 WS-MONTH-NUM               PIC X(2).
              10 WS-MONTH-PRO-CODE          PIC X(3).
              10 WS-MONTH-INC               PIC 9(7)V99.

       01  RPT-FORMAT.
           05 RPT-DETAIL.
              10 RPT-PRO-CODE             PIC X(3).
              10 FILLER                   PIC X(3) VALUE SPACE.
              10 RPT-PRO-NAME             PIC X(20).
              10 RPT-TOTAL-INC            PIC $$$,$$$,$$9.99.


           05 RPT-HEADER                  PIC X(38) 
                    VALUE "CODE  NAME                TOTAL INCOME".
            
       


       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT 
           PERFORM 3000-END THRU 3000-EXIT
           
           
           GOBACK
           .

       1000-INITIAL.
           PERFORM 1100-OPEN-PROV-FILE THRU 1100-EXIT
           PERFORM 8100-READ-PRO THRU 8100-EXIT 
           
           PERFORM 1200-OPEN-MONTH-FILE THRU 1200-EXIT
           PERFORM 8200-READ-MONTH THRU 8200-EXIT 

           PERFORM 1300-OPEN-OUTPUT-FILE THRU 1300-EXIT

           .


       1000-EXIT.
           EXIT.

       1100-OPEN-PROV-FILE.
           OPEN INPUT 100-PROV-INPUT
           IF FILE-OK OF WS-PROV-INPUT-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
              DISPLAY "* PARA 1100-OPEN-PROV-FILE FAILE*"
              UPON CONSOLE 
              DISPLAY "* FILE STATUS : " WS-PROV-INPUT-STATUS " *"
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
           END-IF
           .

       1100-EXIT.
           EXIT.



       1200-OPEN-MONTH-FILE.
           OPEN INPUT 200-MONTH-INPUT 
           IF FILE-OK OF WS-MONTH-INPUT-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
              DISPLAY "* PARA 1200-OPEN-MONTH-FILE FAILE*"
              UPON CONSOLE 
              DISPLAY "* FILE STATUS : " WS-MONTH-INPUT-STATUS " *"
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
           END-IF
           .

       1200-EXIT.
           EXIT.

       
       1300-OPEN-OUTPUT-FILE.
           OPEN OUTPUT 300-INC-OUTPUT 
           IF FILE-OK OF WS-INC-OUTPUT-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
              DISPLAY "* PARA 1300-OPEN-OUTPUT-FILE FAILE*"
              UPON CONSOLE 
              DISPLAY "* FILE STATUS : " WS-INC-OUTPUT-STATUS " *"
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
           END-IF
           .

       1300-EXIT.
           EXIT.


       2000-PROCESS.
           PERFORM 4000-LOAD THRU 4000-EXIT 
           UNTIL  FILE-AT-END OF WS-PROV-INPUT-STATUS
           

           PERFORM 2100-PROCESS-MONTH THRU 2100-EXIT
           UNTIL  FILE-AT-END OF WS-MONTH-INPUT-STATUS 
           
           .


       2000-EXIT.
           EXIT.
       
       2100-PROCESS-MONTH.
      *    DISPLAY MONTH-NUM " " MONTH-PRO-CODE 
      *    " " MONTH-INC 

           MOVE MONTH-NUM TO WS-MONTH-NUM 
           MOVE MONTH-PRO-CODE TO WS-MONTH-PRO-CODE 
           MOVE MONTH-INC TO WS-MONTH-INC 
           
      *    DISPLAY WS-MONTH-NUM " " WS-MONTH-PRO-CODE 
      *    " " WS-MONTH-INC 

      *    Update income total
           PERFORM VARYING IDX-PRO FROM 1 BY 1 UNTIL IDX-PRO > 6
              IF WS-MONTH-PRO-CODE = WS-PRO-CODE(IDX-PRO)
                 ADD WS-MONTH-INC TO WS-INCOME-TOTAL(IDX-PRO)
                 EXIT PERFORM 
              END-IF
           END-PERFORM
           
              
           
           PERFORM 8200-READ-MONTH THRU 8200-EXIT
           .

       2100-EXIT.
           EXIT.



       3000-END.
           PERFORM 3100-END-REPORT THRU 3100-EXIT
           PERFORM 3200-END-COUNT THRU 3200-EXIT
         
           
           CLOSE 100-PROV-INPUT 200-MONTH-INPUT 300-INC-OUTPUT 
           .

       3000-EXIT.
           EXIT.
       
       3100-END-REPORT.
           DISPLAY RPT-HEADER
           MOVE RPT-HEADER TO INC-OUTPUT-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT
           PERFORM VARYING IDX-PRO FROM 1 BY 1 UNTIL IDX-PRO > 6
      *       DISPLAY WS-PRO-CODE(IDX-PRO) " "
      *               WS-PRO-NAME(IDX-PRO) " "
      *               WS-INCOME-TOTAL(IDX-PRO) 
              MOVE WS-PRO-CODE(IDX-PRO) TO RPT-PRO-CODE 
              MOVE WS-PRO-NAME(IDX-PRO) TO RPT-PRO-NAME 
              MOVE WS-INCOME-TOTAL(IDX-PRO) TO RPT-TOTAL-INC
              DISPLAY RPT-DETAIL
              MOVE RPT-DETAIL TO INC-OUTPUT-RECORD 
              PERFORM 7000-WRITE THRU 7000-EXIT  
           END-PERFORM
           .
       3100-EXIT.
           EXIT.

       3200-END-COUNT.
           DISPLAY "Read count from 100-PROV-INPUT : "
                    WS-PROV-INPUT-COUNT 
           
           DISPLAY "Read count from 200-MONTH-INPUT : "
                    WS-MONTH-INPUT-COUNT 

           .


       3200-EXIT.
           EXIT.
       4000-LOAD.
           MOVE PRO-CODE  TO WS-PRO-CODE(IDX-PRO)
           MOVE PRO-NAME  TO WS-PRO-NAME(IDX-PRO) 
           ADD 1 TO IDX-PRO
           PERFORM 8100-READ-PRO THRU 8100-EXIT
           .


       4000-EXIT.
           EXIT.


       7000-WRITE.
           WRITE INC-OUTPUT-RECORD
           IF FILE-OK OF WS-INC-OUTPUT-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
              DISPLAY "* PARA 7000-WRITE FAILE*"
              UPON CONSOLE 
              DISPLAY "* FILE STATUS : " WS-INC-OUTPUT-STATUS " *"
              DISPLAY "***** MONTHIN ABEND *****"
              UPON CONSOLE 
           END-IF
           .


       7000-EXIT.
           EXIT.



       8100-READ-PRO.
           READ 100-PROV-INPUT
           IF FILE-OK OF WS-PROV-INPUT-STATUS
              ADD 1 TO WS-PROV-INPUT-COUNT 
           ELSE
              IF FILE-AT-END OF WS-PROV-INPUT-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY "***** MONTHIN ABEND *****"
                 UPON CONSOLE 
                 DISPLAY "* PARA 8100-READ-PRO FAILE*"
                 UPON CONSOLE 
                 DISPLAY "* FILE STATUS : " WS-PROV-INPUT-STATUS " *"
                 DISPLAY "***** MONTHIN ABEND *****"
                 UPON CONSOLE 
              END-IF 
           END-IF 
           .


       8100-EXIT.
           EXIT.

       8200-READ-MONTH.
           READ 200-MONTH-INPUT
           IF FILE-OK OF WS-MONTH-INPUT-STATUS
              ADD 1 TO WS-MONTH-INPUT-COUNT 
           ELSE
              IF FILE-AT-END OF WS-MONTH-INPUT-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY "***** MONTHIN ABEND *****"
                 UPON CONSOLE 
                 DISPLAY "* PARA 8200-READ-MONTH FAILE*"
                 UPON CONSOLE 
                 DISPLAY "* FILE STATUS : " WS-MONTH-INPUT-STATUS " *"
                 DISPLAY "***** MONTHIN ABEND *****"
                 UPON CONSOLE 
              END-IF 
           END-IF   
           .


       8200-EXIT.
           EXIT.

